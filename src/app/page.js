"use client";
import React, { useState } from "react";
import { Container, Typography, Paper, Grid, Box } from "@mui/material";
import { Buscar } from "../componentes/Buscar";
import { Ordenar } from "../componentes/Ordenar";
import { Clasificar } from "../componentes/Clasificar";
export default function Home() {
  return (
    <>
      <Box
        sx={{
          backgroundImage: 'url("/portada1.jpg")',
          backgroundSize: "cover",
          backgroundPosition: "center",
          height: "100vh", // Ajusta la altura según tus necesidades
          display: "flex",
          flexDirection: "column",
          alignItems: "center",
          justifyContent: "flex-start",
        }}
      >
        <Container className="pt-10">
          <Paper
            elevation={24} // Ajusta la elevación según tus necesidades
            sx={{
              padding: 2,
              borderRadius: 16, // Ajusta el radio de las esquinas según tus necesidades
            }}
          >
            <Typography
              className="text-center"
              variant="h3"
              color="primary"
              sx={{ mt: 0 }}
            >
              Bienvenido al Noticiero EL HOCICON
            </Typography>
          </Paper>
          <Grid container spacing={3} mt={3}>
            <Grid item xs={12} sm={4}>
              <Buscar />
            </Grid>
            <Grid item xs={12} sm={4}>
              <Ordenar />
            </Grid>
            <Grid item xs={12} sm={4}>
              <Clasificar />
            </Grid>
          </Grid>
        </Container>
      </Box>
    </>
  );
}
