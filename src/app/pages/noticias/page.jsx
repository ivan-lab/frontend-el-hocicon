"use client";
import React, { useState } from "react";
import axios from "axios";
import { Noticias } from "../../../componentes/Noticias";
import Button from "@mui/material/Button";
import Box from "@mui/material/Box";

export default function NoticiasPages() {
  const [noticias, setNoticias] = useState([]);
  const [getError, setGetError] = useState(null);

  React.useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await axios.get(
          `${process.env.NEXT_PUBLIC_BASE_URL_BACKEND}`
        );

        setGetError(null);
        setNoticias(response.data.slice(0, 3));
      } catch (error) {
        if (error.response) {
          const { status, data } = error.response;
          if (status === 400) {
            setNoticias(null);
            setGetError(`RS: ${data.message}`);
            setGetError(`RS: ${data.error}`);
          } else if (status === 500) {
            setNoticias(null);
            setGetError(`RS: ${data.message}`);
            setGetError(`RS: ${data.error}`);
          }
        } else if (error.request) {
          setGetError("RF: No se pudo obtener respuesta del servidor");
        } else {
          setGetError("RF: Error al enviar la solicitud");
        }
      }
    };

    fetchData();
  }, []);

  const cargarMasNoticias = async () => {
    try {
      const response = await axios.get("http://localhost:3000/noticia");
      setGetError(null);
      setNoticias([...noticias, ...response.data.slice(3, 6)]);
    } catch (error) {
      if (error.response) {
        const { status, data } = error.response;
        if (status === 400) {
          setNoticias(null);
          setGetError(`RS: ${data.message}`);
          setGetError(`RS: ${data.error}`);
        } else if (status === 500) {
          setNoticias(null);
          setGetError(`RS: ${data.message}`);
          setGetError(`RS: ${data.error}`);
        }
      } else if (error.request) {
        setGetError("RF: No se pudo obtener respuesta del servidor");
      } else {
        setGetError("RF: Error al enviar la solicitud");
      }
    }
  };

  return (
    <>
      <Box display="flex" justifyContent="center" alignItems="center">
        <div className="py-5">
          {getError && (
            <p className="text-red-700 text-center py-5">{getError}</p>
          )}
          {noticias && (
            <>
              {noticias.map((noticia) => (
                <Noticias key={noticia.id} noticia={noticia} />
              ))}
              <br />
              <Button variant="outlined" onClick={cargarMasNoticias}>
                Cargar más noticias
              </Button>
              <br />
            </>
          )}
        </div>
      </Box>
    </>
  );
}
