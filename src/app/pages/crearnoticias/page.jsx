"use client";
import React, { useState } from "react";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";
import Box from "@mui/material/Box";
import TextField from "@mui/material/TextField";
import Grid from "@mui/material/Grid";
import axios from "axios";
import Card from "@mui/material/Card";
import { Noticias } from "../../../componentes/Noticias";
export default function CrearnoticiasPages() {
  const [create, setCreate] = useState(null);
  const [createError, setCreateError] = useState(null);
  const [formValues, setFormValues] = useState({
    titulo: "",
    lugar: "",
    autor: "",
    contenido: "",
  });

  const handleChange = (event) => {
    const { id, value } = event.target;
    setFormValues((prevValues) => ({
      ...prevValues,
      [id]: value,
    }));
  };

  const handleSubmit = async (event) => {
    event.preventDefault();

    try {
      const response = await axios.post(
        `${process.env.NEXT_PUBLIC_BASE_URL_BACKEND}`,
        formValues
      );
      setCreateError(null);
      setCreate(response.data);
    } catch (error) {
      if (error.response) {
        const { status, data } = error.response;
        if (status === 400) {
          setCreate(null);
          setCreateError(`RS: ${data.message}`);
          setCreateError(`RS: ${data.error}`);
        } else if (status === 500) {
          setCreate(null);
          setCreateError(`RS: ${data.message}`);
          setCreateError(`RS: ${data.error}`);
        }
      } else if (error.request) {
        setCreateError("RF: No se pudo obtener respuesta del servidor");
      } else {
        setCreateError("RF: Error al enviar la solicitud");
      }
    }
  };
  console.log(create);
  return (
    <>
      <Box display="flex" justifyContent="center" alignItems="center">
        <div className="py-10">
          {createError && (
            <p className="text-red-700 text-center py-5">{createError}</p>
          )}
          <Card
            sx={{
              maxWidth: { xs: 345, md: 900 },
              margin: "auto", // Centra el card
            }}
            elevation={24}
          >
            <form onSubmit={handleSubmit} className="px-5">
              <Typography className="text-center" variant="h3" gutterBottom>
                Publicar Noticia
              </Typography>
              <Grid container spacing={2}>
                <Grid item xs={12} md={12}>
                  <TextField
                    required
                    id="titulo"
                    label="Título"
                    variant="standard"
                    fullWidth
                    value={formValues.titulo}
                    onChange={handleChange}
                  />
                </Grid>
                <Grid item xs={12} md={6}>
                  <TextField
                    id="lugar"
                    label="Lugar"
                    variant="standard"
                    fullWidth
                    value={formValues.lugar}
                    onChange={handleChange}
                  />
                </Grid>
                <Grid item xs={12} md={6}>
                  <TextField
                    id="autor"
                    label="Autor"
                    variant="standard"
                    fullWidth
                    value={formValues.autor}
                    onChange={handleChange}
                  />
                </Grid>

                <Grid item xs={12}>
                  <TextField
                    id="contenido"
                    label="Contenido"
                    multiline
                    rows={4}
                    variant="standard"
                    fullWidth
                    value={formValues.contenido}
                    onChange={handleChange}
                  />
                </Grid>
              </Grid>
              <br />
              <div className="p-5">
                <Button type="submit" variant="outlined" color="primary">
                  Enviar
                </Button>
              </div>
            </form>
          </Card>
        </div>
      </Box>
      <Box display="flex" justifyContent="center" alignItems="center">
        {create && (
          <div div className="py-5">
            <Typography className="text-center" variant="h5" gutterBottom>
              Noticia Creada
            </Typography>
            <Box mt={2}>
              <Noticias key={create.id} noticia={create} />
            </Box>
          </div>
        )}
      </Box>
    </>
  );
}
