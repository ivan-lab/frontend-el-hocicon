"use client";
import React, { useState, useEffect } from "react";
import axios from "axios";
import Button from "@mui/material/Button";
import TextField from "@mui/material/TextField";
import Box from "@mui/material/Box";
import Grid from "@mui/material/Grid";
import Card from "@mui/material/Card";
import Typography from "@mui/material/Typography";
import { Noticias } from "../../../componentes/Noticias";
export default function EditarnoticiasPages() {
  const [update, setUpdate] = useState(null);
  const [updateError, setUpdateError] = useState(null);
  const [formValues, setFormValues] = useState({
    titulo: "",
    imagen: "",
    lugar: "",
    autor: "",
    contenido: "",
  });

  const [noticiaId, setNoticiaId] = useState(""); // Estado para almacenar el ID de la noticia
  const [loading, setLoading] = useState(false);

  const handleChange = (event) => {
    const { id, value } = event.target;
    setFormValues((prevValues) => ({
      ...prevValues,
      [id]: value,
    }));
  };

  const handleSubmit = async (event) => {
    event.preventDefault();

    try {
      setLoading(true);
      const response = await axios.patch(
        `${process.env.NEXT_PUBLIC_BASE_URL_BACKEND}${noticiaId}`,
        formValues
      );
      setUpdateError(null);
      setUpdate(response.data);
    } catch (error) {
      if (error.response) {
        const { status, data } = error.response;
        if (status === 400) {
          setUpdate(null);
          setUpdateError(`RS: ${data.message}`);
          setUpdateError(`RS: ${data.error}`);
        } else if (status === 500) {
          setUpdate(null);
          setUpdateError(`RS: ${data.message}`);
          setUpdateError(`RS: ${data.error}`);
        }
      } else if (error.request) {
        setUpdateError("RF: No se pudo obtener respuesta del servidor");
      } else {
        setUpdateError("RF: Error al enviar la solicitud");
      }
    } finally {
      setLoading(false);
    }
  };

  return (
    <>
      <Box display="flex" justifyContent="center" alignItems="center">
        <div className="py-10">
          {updateError && (
            <p className="text-red-700 text-center py-5">{updateError}</p>
          )}
          <Card
            sx={{
              maxWidth: { xs: 345, md: 900 },
              margin: "auto", // Centra el card
            }}
            elevation={24}
          >
            <form onSubmit={handleSubmit} className="px-5">
              <Typography className="text-center" variant="h3" gutterBottom>
                Actualizar Noticia
              </Typography>
              <Grid container spacing={2}>
                <Grid item xs={12} md={12}>
                  <TextField
                    required
                    id="id" // Campo para ingresar el ID
                    label="ID"
                    variant="standard"
                    fullWidth
                    value={noticiaId}
                    onChange={(event) => setNoticiaId(event.target.value)}
                  />
                </Grid>

                <Grid item xs={12} md={12}>
                  <TextField
                    required
                    id="titulo"
                    label="Título"
                    variant="standard"
                    fullWidth
                    value={formValues.titulo}
                    onChange={handleChange}
                  />
                </Grid>

                <Grid item xs={12} md={6}>
                  <TextField
                    id="imagen"
                    label="Imagen"
                    variant="standard"
                    fullWidth
                    value={formValues.imagen}
                    onChange={handleChange}
                  />
                </Grid>
                <Grid item xs={12} md={6}>
                  <TextField
                    id="lugar"
                    label="Lugar"
                    variant="standard"
                    fullWidth
                    value={formValues.lugar}
                    onChange={handleChange}
                  />
                </Grid>
                <Grid item xs={12} md={6}>
                  <TextField
                    id="autor"
                    label="Autor"
                    variant="standard"
                    fullWidth
                    value={formValues.autor}
                    onChange={handleChange}
                  />
                </Grid>
                <Grid item xs={12}>
                  <TextField
                    id="contenido"
                    label="Contenido"
                    multiline
                    rows={4}
                    variant="standard"
                    fullWidth
                    value={formValues.contenido}
                    onChange={handleChange}
                  />
                </Grid>
              </Grid>
              <br />
              <div className="p-5">
                <Button
                  type="submit"
                  variant="outlined"
                  color="primary"
                  disabled={loading}
                >
                  {loading ? "Actualizando..." : "Actualizar"}
                </Button>
              </div>
            </form>
          </Card>
        </div>
      </Box>
      <Box display="flex" justifyContent="center" alignItems="center">
        {update && (
          <div div className="py-5">
            <Typography className="text-center" variant="h5" gutterBottom>
              Noticia Actualizada
            </Typography>
            <Box mt={2}>
              <Noticias key={update.id} noticia={update} />
            </Box>
          </div>
        )}
      </Box>
    </>
  );
}
