"use client";
import React, { useState } from "react";
import axios from "axios";
import Button from "@mui/material/Button";
import TextField from "@mui/material/TextField";
import Box from "@mui/material/Box";
import Card from "@mui/material/Card";
import Typography from "@mui/material/Typography";
import { Noticias } from "../../../componentes/Noticias";
export default function EliminarnoticiasPages() {
  const [deleteError, setDeleteError] = useState(null);
  const [noticiaId, setNoticiaId] = useState("");
  const [deletedNoticia, setDeletedNoticia] = useState(null);
  const [loading, setLoading] = useState(false);

  const handleDelete = async () => {
    try {
      setLoading(true);
      const response = await axios.delete(
        `${process.env.NEXT_PUBLIC_BASE_URL_BACKEND}${noticiaId}`
      );
      setDeleteError(null);
      setDeletedNoticia(response.data);
      setTimeout(() => {
        setDeletedNoticia(null);
      }, 5000);
    } catch (error) {
      if (error.response) {
        const { status, data } = error.response;
        if (status === 400) {
          setDeletedNoticia(null);
          setDeleteError(`RS: ${data.message}`);
          setDeleteError(`RS: ${data.error}`);
        } else if (status === 500) {
          setDeletedNoticia(null);
          setDeleteError(`RS: ${data.message}`);
          setDeleteError(`RS: ${data.error}`);
        }
      } else if (error.request) {
        setDeleteError("RF: No se pudo obtener respuesta del servidor");
      } else {
        setDeleteError("RF: Error al enviar la solicitud");
      }
    } finally {
      setLoading(false);
    }
  };

  return (
    <Box display="flex" justifyContent="center" alignItems="center">
      <div className="py-10">
        {deleteError && (
          <p className="text-red-700 text-center py-5">{deleteError}</p>
        )}
        <Card
          sx={{
            maxWidth: { xs: 345, md: 900 },
          }}
          elevation={24}
        >
          <div className="px-5">
            <Typography className="text-center" variant="h3" gutterBottom>
              Eliminar Noticia
            </Typography>
            <TextField
              required
              id="noticiaId"
              label="ID de Noticia"
              variant="standard"
              fullWidth
              value={noticiaId}
              onChange={(event) => setNoticiaId(event.target.value)}
            />
            <div className="p-5">
              <Button
                variant="outlined"
                color="secondary"
                onClick={handleDelete}
                disabled={!noticiaId || loading}
              >
                {loading ? "Eliminando..." : "Eliminar Noticia"}
              </Button>
            </div>
          </div>
        </Card>
        {deletedNoticia && (
          <div div className="py-10">
            <Typography className="text-center" variant="h5" gutterBottom>
              Noticia Eliminada
            </Typography>
            <Box mt={2}>
              <Noticias key={deletedNoticia.id} noticia={deletedNoticia} />
            </Box>
          </div>
        )}
      </div>
    </Box>
  );
}
