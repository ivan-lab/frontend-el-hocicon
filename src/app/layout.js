import * as React from "react";

import { Inter } from "next/font/google";
import "./globals.css";

import { NavBar } from "../componentes/NavBar";

const inter = Inter({ subsets: ["latin"] });

export const metadata = {
  title: "El Hocicon",
  description: "CRUD el hocicon",
};

export default function RootLayout({ children }) {
  return (
    <html lang="es">
      <body className={inter.className}>
        <NavBar />
        {children}
      </body>
    </html>
  );
}
