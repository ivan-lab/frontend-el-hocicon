"use client";
import React, { useState } from "react";
import axios from "axios";
import Button from "@mui/material/Button";
import TextField from "@mui/material/TextField";
import Grid from "@mui/material/Grid";
import Card from "@mui/material/Card";
import Typography from "@mui/material/Typography";
import CardContent from "@mui/material/CardContent";
import { Noticias } from "../componentes/Noticias";
export function Buscar() {
  const [buscarError, setBuscarError] = useState(null);
  const [buscar, setBuscar] = useState(null);
  const [inputBuscar, setInputBuscar] = useState("");

  const handleSearch = async () => {
    try {
      const response = await axios.get(
        `${process.env.NEXT_PUBLIC_BASE_URL_BACKEND}titulo/${inputBuscar}`
      );
      setBuscarError(null);
      setBuscar(response.data);
    } catch (error) {
      if (error.response) {
        const { status, data } = error.response;
        if (status === 400) {
          setBuscar(null);
          setBuscarError(`RS: ${data.message}`);
          setBuscarError(`RS: ${data.error}`);
        } else if (status === 500) {
          setBuscar(null);
          setBuscarError(`RS: ${data.message}`);
          setBuscarError(`RS: ${data.error}`);
        }
      } else if (error.request) {
        setBuscarError("RF: No se pudo obtener respuesta del servidor");
      } else {
        setBuscarError("RF: Error al enviar la solicitud");
      }
    }
  };

  return (
    <Card>
      <CardContent>
        <Typography variant="h5" color="primary">
          Buscar
        </Typography>
        <Grid className="pt-5" container spacing={2} alignItems="center">
          <Grid item>
            <TextField
              label="Título de la Noticia"
              variant="standard"
              fullWidth
              value={inputBuscar}
              onChange={(e) => setInputBuscar(e.target.value)}
            />
          </Grid>
          <Grid item>
            <Button variant="outlined" color="primary" onClick={handleSearch}>
              Buscar Noticia
            </Button>
          </Grid>
        </Grid>
        {buscarError && (
          <p className="text-red-700 text-center py-5">{buscarError}</p>
        )}
        {buscar && (
          <div className="py-10">
            <Typography color="primary" variant="h6">
              Noticia Encontrada:
            </Typography>
            {buscar.map((buscar) => (
              <Noticias key={buscar.id} noticia={buscar} />
            ))}
          </div>
        )}
      </CardContent>
    </Card>
  );
}
