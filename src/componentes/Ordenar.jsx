"use client";
import React, { useState, useEffect } from "react";
import axios from "axios";
import Button from "@mui/material/Button";
import TextField from "@mui/material/TextField";
import Grid from "@mui/material/Grid";
import Card from "@mui/material/Card";
import Typography from "@mui/material/Typography";
import CardContent from "@mui/material/CardContent";
import Box from "@mui/material/Box";
import InputLabel from "@mui/material/InputLabel";
import MenuItem from "@mui/material/MenuItem";
import FormControl from "@mui/material/FormControl";
import Select from "@mui/material/Select";
import { Noticias } from "../componentes/Noticias";
export function Ordenar() {
  const [buscarError, setBuscarError] = useState(null);
  const [buscar, setBuscar] = useState(null);
  const [tipo, setTipo] = useState("");

  const handleChange = (event) => {
    setTipo(event.target.value);
    handleFetchData();
  };
  const handleFetchData = async () => {
    try {
      const response = await axios.get(`http://localhost:3000/noticia/${tipo}`);
      setBuscarError(null);
      setBuscar(response.data);
    } catch (error) {
      if (error.response) {
        const { status, data } = error.response;
        if (status === 400) {
          setBuscar(null);
          setBuscarError(`RS: ${data.message}`);
          setBuscarError(`RS: ${data.error}`);
        } else if (status === 500) {
          setBuscar(null);
          setBuscarError(`RS: ${data.message}`);
          setBuscarError(`RS: ${data.error}`);
        }
      } else if (error.request) {
        setBuscarError("RF: No se pudo obtener respuesta del servidor");
      } else {
        setBuscarError("RF: Error al enviar la solicitud");
      }
    }
  };

  return (
    <Card>
      <CardContent>
        <Typography variant="h5" color="primary">
          Ordenar
        </Typography>
        {buscarError && (
          <p className="text-red-700 text-center py-5">{buscarError}</p>
        )}
        <Box sx={{ minWidth: 120 }}>
          <FormControl fullWidth>
            <InputLabel className="" id="demo-simple-select-label">
              Seleecionar
            </InputLabel>
            <Select value={tipo} label="Seleecionar" onChange={handleChange}>
              <MenuItem value="recientes">Mas Antiguas</MenuItem>
              <MenuItem value="antiguas">Mas Recientes</MenuItem>
            </Select>
          </FormControl>
        </Box>
        <br />
        {buscar && (
          <div>
            <Typography color="primary" variant="h6">
              Noticias Encontradas:
            </Typography>
            {buscar.map((noticia) => (
              <>
                <Noticias key={noticia.id} noticia={noticia} />
              </>
            ))}
          </div>
        )}
      </CardContent>
    </Card>
  );
}
