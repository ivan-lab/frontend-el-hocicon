"use client";
import React, { useState, useEffect } from "react";
import axios from "axios";
import Card from "@mui/material/Card";
import Typography from "@mui/material/Typography";
import CardContent from "@mui/material/CardContent";
import InputLabel from "@mui/material/InputLabel";
import MenuItem from "@mui/material/MenuItem";
import FormControl from "@mui/material/FormControl";
import Select from "@mui/material/Select";
import Box from "@mui/material/Box";

import { Noticias } from "../componentes/Noticias";
export function Clasificar() {
  const [buscarError, setBuscarError] = useState(null);
  const [buscar, setBuscar] = useState(null);
  const [lugar, setLugar] = useState("");

  const handleFetchData = async () => {
    try {
      const response = await axios.get(
        `${process.env.NEXT_PUBLIC_BASE_URL_BACKEND}lugar/${lugar}`
      );
      setBuscarError(null);
      setBuscar(response.data);
    } catch (error) {
      if (error.response) {
        const { status, data } = error.response;
        if (status === 400) {
          setBuscar(null);
          setBuscarError(`RS: ${data.message}`);
          setBuscarError(`RS: ${data.error}`);
        } else if (status === 500) {
          setBuscar(null);
          setBuscarError(`RS: ${data.message}`);
          setBuscarError(`RS: ${data.error}`);
        }
      } else if (error.request) {
        setBuscarError("RF: No se pudo obtener respuesta del servidor");
      } else {
        setBuscarError("RF: Error al enviar la solicitud");
      }
    }
  };
  const handleChange = (event) => {
    const selectedLugar = event.target.value;
    setLugar(selectedLugar);
  };

  useEffect(() => {
    if (lugar !== "") {
      handleFetchData();
    }
  }, [lugar]);
  return (
    <Card>
      <CardContent>
        <Typography variant="h5" color="primary">
          Clasificar
        </Typography>
        {buscarError && (
          <p className="text-red-700 text-center py-5">{buscarError}</p>
        )}
        <Box sx={{ minWidth: 120 }}>
          <FormControl fullWidth>
            <InputLabel  id="demo-simple-select-label">
              Seleecionar
            </InputLabel>
            <Select value={lugar} label="Seleecionar" onChange={handleChange}>
              <MenuItem value="La Paz">La Paz</MenuItem>
              <MenuItem value="El Alto">El Alto</MenuItem>
              <MenuItem value="Cochabamba">Cochabamba</MenuItem>
              <MenuItem value="Santa Cruz">Santa Cruz</MenuItem>
              <MenuItem value="Beni">Beni</MenuItem>
              <MenuItem value="Pando">Pando</MenuItem>
              <MenuItem value="Sucre">Sucre</MenuItem>
              <MenuItem value="Oruro">Oruro</MenuItem>
              <MenuItem value="Postosi">Postosi</MenuItem>
              <MenuItem value="Chuquisaca">Chuquisaca</MenuItem>
            </Select>
          </FormControl>
        </Box>
        {buscar && (
          <div className="py-5">
            <Typography color="primary" variant="h6">
              Noticias Encontradas:
            </Typography>
            {buscar.map((buscar) => (
              <Noticias key={buscar.id} noticia={buscar} />
            ))}
          </div>
        )}
      </CardContent>
    </Card>
  );
}
