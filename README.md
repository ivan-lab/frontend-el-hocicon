El proyecto consta de las paginas:

1. http://localhost:3001/
2. http://localhost:3001/pages/crearnoticias
3. http://localhost:3001/pages/editarnoticias
4. http://localhost:3001/pages/eliminarnoticias
5. http://localhost:3001/pages/noticias

Estas páginas están diseñadas para facilitar las operaciones básicas del CRUD. Es esencial destacar que se requiere tener el ID correspondiente para editar o eliminar noticias.

En la página de inicio, se presentan tres opciones:

1. Buscar:
   En esta sección, se debe ingresar el título de la noticia. Cuanto más preciso sea el título, se mostrará la noticia buscada de manera más exacta.

2. Ordenar:
   Esta opción permite organizar las noticias según la fecha de creación. Se ofrecen dos alternativas:
   2.1. La más reciente
   2.2. La más antigua
   Las noticias se desplegarán en el orden seleccionado.

3. Clasificar
   Aquí, las noticias se clasificarán según los nueve departamentos especificados.

para la instalacion y configuracuon ir al archivo: NSTALL.md
